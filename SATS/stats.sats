(* ****** ****** *)
(*
** For supporting
** basis statical computation
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "ATS-ML"
//
(* ****** ****** *)
//
typedef
List1(a:t@ype) = listGte(a, 1)
//
(* ****** ****** *)
//
typedef
listord(a:t@ype, n:int) = list(a, n)
typedef
Listord0(a:t@ype) = [n:nat] listord(a, n)
typedef
Listord1(a:t@ype) = [n:pos] listord(a, n)
//
(* ****** ****** *)
//
fun
{a:t0p}
gdata_eval(x: a):<> double
//
(* ****** ****** *)
//
fun//{}
list_max
  (xs: List1(double)): double
fun//{}
list_min
  (xs: List1(double)): double
//
fun
{a:t0p}
glist_max(xs: List1(INV(a))): double
fun
{a:t0p}
glist_min(xs: List1(INV(a))): double
//
(* ****** ****** *)
//
fun
{a:t0p}
glistpre_accret
  (xs: List(INV(a)), n0: intGte(0)): double
//
(* ****** ****** *)
//
fun//{}
list_median
  (xs: List1(double)): double
fun//{}
listord_median
  (xs: Listord1(double)): double
//
fun
{a:t0p}
glist_median(xs: List1(INV(a))): double
fun
{a:t0p}
glistord_median(xs: Listord1(INV(a))): double
//
(* ****** ****** *)
//
fun//{}
list_mean
  (xs: List1(double)): double
fun//{}
listpre_mean
  (xs: List1(double), n0: intGte(1)): double
//
fun
{a:t0p}
glist_mean(xs: List1(INV(a))): double
fun
{a:t0p}
glistpre_mean(xs: List1(INV(a)), n0: intGte(1)): double
//
(* ****** ****** *)
//
fun//{}
list_stdev
  (xs: List1(double)): double
fun//{}
listpre_stdev
  (xs: List1(double), n0: intGte(1)): double
//
fun
{a:t0p}
glist_stdev(xs: List1(INV(a))): double
fun
{a:t0p}
glistpre_stdev(xs: List1(INV(a)), n0: intGte(1)): double
//
(* ****** ****** *)
//
fun//{}
list_absdev
  (xs: List1(double)): double
fun//{}
listpre_absdev
  (xs: List1(double), n0: intGte(1)): double
//
fun
{a:t0p}
glist_absdev(xs: List1(INV(a))): double
fun
{a:t0p}
glistpre_absdev(xs: List1(INV(a)), n0: intGte(1)): double
//
(* ****** ****** *)
//
fun
list_maxdrawdown
  (xs: List(double)): double
fun
{a:t@ype}
glist_maxdrawdown(xs: List(INV(a))): double
//
(* ****** ****** *)
//
fun
list_summarize(xs: List(double)): void
//
(* ****** ****** *)
//
fun//{}
list_ratios
  (xs: listGte(double, 1)): stream_vt(double)
fun//{}
list_change_ratios
  (xs: listGte(double, 1)): stream_vt(double)
//
(* ****** ****** *)
//
fun//{}
list_rolling_mean
{n:int}
{k:int | n >= k; k >= 1}
(xs: list(double, n), width: int(k)): stream_vt(double)
//
fun//{}
list_rolling_stdev
{n:int}
{k:int | n >= k; k >= 1}
(xs: list(double, n), width: int(k)): stream_vt(double)
//
(* ****** ****** *)
//
fun
{a:t@ype}
list_rolling_stream
{n:int}
{k:pos | n >= k}
(xs: list(INV(a), n), width: int(k)): stream_vt(listGte(a, k))
//
(* ****** ****** *)

(* end of [stats.sats] *)
