(* ****** ****** *)
(*
** For various utilities
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "ATS-ML"
//
(* ****** ****** *)
//
#staload
"libats/ML/SATS/basis.sats"
//
(* ****** ****** *)
//
fun
{a,b:t0p}
stream_vt_zip
(stream_vt(INV(a)),
 stream_vt(INV(b))): stream_vt(@(a, b))
//
(* ****** ****** *)
//
fun
{a,b:vt0p}
stream2list2_vt
(
  stream_vt(@(INV(a), INV(b)))
) : [n:nat] (list_vt(a, n), list_vt(b, n))
//
(* ****** ****** *)
//
fun
{a:t@ype}
stream_vt_stepize
(
xs: stream_vt(INV(a)), step: intGte(0)
) : stream_vt(a) // end of [stream_vt_stepize]
//
(* ****** ****** *)
//
fun
fileref_csv_parse_line_nerr
  (inp: FILEref, nerr: &int >> _): List0_vt(gvhashtbl)
//
fun
keys_lines_csv_parse_line_nerr
( keys: list0(string)
, lines: stream_vt(string), nerr: &int >> _): List0_vt(gvhashtbl)
//
(* ****** ****** *)

(* end of [utils.sats] *)
