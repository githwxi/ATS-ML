(* ****** ****** *)
(*
** For testing stats-functions
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#staload "./../SATS/stats.sats"

(* ****** ****** *)

val
xs_a =
g0ofg1
(
$list{dbl}
(1., 2., 3., 4., 5., 6.)
) (* end of [val] *)

(* ****** ****** *)

val avga = list0_mean(xs_a)
val vrna = list0_variance(xs_a)

(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println!
("sqrti(4) = ", sqrti(4))
val () =
println!
("sqrtd(4.) = ", sqrtd(4.))
//
val () = println! ("xs_a = ", xs_a)
val () = println! ("mean(A) = ", avga)
val () = println! ("variance(A) = ", vrna)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test01.dats] *)
