(* ****** ****** *)
//
// For various
// statistical functions
//
(* ****** ****** *)
//
#staload
"libats/ML/SATS/basis.sats"
//
(* ****** ****** *)
(*
** listord for ordered list
*)
(* ****** ****** *)

typedef dbl = double
typedef dblist0 = list0(dbl)

(* ****** ****** *)
//
typedef
cmpfun(a:t@ype) = (a, a) -<fun1> int
typedef
cmpcfun(a:t@ype) = (a, a) -<cloref1> int
//
(* ****** ****** *)

fun
sqrtd : double -> double
fun
sqrti : ( int ) -> double

(* ****** ****** *)
//
fun
{a:t@ype}
list0_merge
(xs: list0(a), ys: list0(a)): list0(a)
//
fun
{a:t@ype}
list0_mergesort(xs: list0(a)): list0(a)
//
(* ****** ****** *)

fun
list0_mean(A: dblist0): double
fun
list0_median(A: dblist0): double

(* ****** ****** *)

fun
list0_stdev(xs: dblist0): double
fun
list0_variance(xs: dblist0): double

(* ****** ****** *)
//
fun
{a:t@ype}
list0_freq(xs: list0(a)): list0($tup(a, double))
//
(* ****** ****** *)

(* end of [stats.sats] *)
