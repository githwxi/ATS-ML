(* ****** ****** *)
//
// For various
// statistical functions
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
#staload M =
"libats/libc/SATS/math.sats"
#staload _ =
"libats/libc/DATS/math.dats"
//
(* ****** ****** *)

#staload "./../SATS/stats.sats"

(* ****** ****** *)

implement sqrtd(x) = $M.sqrt(x)
implement sqrti(x) = sqrtd(g0int2float(x))

(* ****** ****** *)

implement
list0_mean(xs) = let
  val n0 = length(xs)
  val () = assertloc(n0 >= 1)
in
  list0_foldleft<dbl><dbl>(xs, 0.0, lam(res, x) => res+x)/n0
end // end of [list0_mean]

(* ****** ****** *)

implement
list0_stdev(xs) = let
  val n0 = length(xs)
  val () = assertloc(n0 >= 1)
  val mu = list0_mean(xs)
  fun{} sq(x: double): double = x*x
in
//
sqrtd
(
list0_foldleft<dbl><dbl>(xs, 0.0, lam(res, x) => res+sq(x-mu))/(n0)
)
//
end // end of [list0_stdev]

(* ****** ****** *)

implement
list0_variance(xs) = let
  val n0 = length(xs)
  val () = assertloc(n0 >= 1)
  val mu = list0_mean(xs)
  fun{} sq(x: double): double = x*x
in
  list0_foldleft<dbl><dbl>(xs, 0.0, lam(res, x) => res+sq(x-mu))/(n0)
end // end of [list0_variance]

(* ****** ****** *)

(* end of [stats.dats] *)
