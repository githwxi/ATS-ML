(* ****** ****** *)
(*
** For getting index data
** For getting stock data
*)
(* ****** ****** *)
//
#define
LIBATSCC2JS_targetloc
"$PATSHOME/contrib\
/libatscc2js/ATS2-0.3.2"
//
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
#define
ATS_PACKNAME "GetData"
#define
ATS_EXTERN_PREFIX "GetData__"
#define
ATS_STATIC_PREFIX "_GetData__"
//
(* ****** ****** *)
//
#staload
UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)
//
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
//
(* ****** ****** *)
//
extern
fun
line_get_at
( line: string
, delim: char, i0: intGte(0)
) : Option_vt(string) = "mac#GetData__line_get_at"
//
(* ****** ****** *)
//
implement
line_get_at
(
  line, delim, i0
) = let
//
val
[n:int]
line = g1ofg0(line)
//
val n0 = length(line)
//
fun
aux0
{j:nat | j <= n}
(
 i: int, j: int(j)
) : intLte(n) =
if (i < i0) then aux1(i, j) else j
and
aux1
{j:nat | j <= n}
(
 i: int, j: int(j)
) : intLte(n) =
(
if
(j < n0)
then let
  val c0 = line.charCodeAt(j)
in
//
if c0 != delim
  then aux1(i, j+1) else aux0(i+1, j+1)
//
end else (~1) (* end of [else] *)
)
//
fun
aux2
{j:nat | j <= n}
(
 j: int(j)
) : intBtwe(j, n) =
if
(j < n0)
then let
  val c0 = line.charCodeAt(j)
in
//
if c0 != delim then aux2(j+1) else j
//
end else (n0) (* end of [else] *)
//
val j0 = aux0(0, 0)
//
in
//
if
(j0 >= 0)
then
Some_vt(string_substring_beg_end(line, j0, aux2(j0)))
else None_vt()
//
end // end of [line_get_at]

(* ****** ****** *)
//
implement
dateretd_get_date(x) =
let val+DATERETD(date, _) = x in date end
implement
dateretd_get_retd(x) =
let val+DATERETD(_, retd) = x in retd end
//
implement
datevald_get_date(x) =
let val+DATEVALD(date, _) = x in date end
implement
datevald_get_vald(x) =
let val+DATEVALD(_, vald) = x in vald end
//
(* ****** ****** *)

implement
stream_vt_datevald2retd
  (xs) = let
//
fun
auxmain
(
v0: double,
xs: stream_vt(datevald)
) : stream_vt(dateretd) = $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
   stream_vt_nil()
| ~stream_vt_cons(x1, xs) => let
    val v1 = x1.vald()
    val rd =
      DATERETD(x1.date(), v1/v0-1.0)
    // end of [val]
  in
    stream_vt_cons(rd, auxmain(v1, xs))
  end
) , lazy_vt_free(xs)
)
//
in
  case+ !xs of
  | ~stream_vt_nil() => stream_vt_make_nil()
  | ~stream_vt_cons(x0, xs) => auxmain(x0.vald(), xs)
end // end of [stream_vt_datevald2retd]

(* ****** ****** *)

(* end of [GetData.dats] *)
