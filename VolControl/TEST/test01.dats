(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
MATH =
"libats/libc/SATS/math.sats"
//
staload _ =
"libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
#staload "./../DATS/GetData.dats"
//
(* ****** ****** *)

#define COMMA ','

(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println! ("Hello from [test01]!")
//
val
line =
"Date,Open,High,Low,Close,Adj Close,Volume"
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 0)
val ((*void*)) = println!("item(0) = ", item)
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 1)
val ((*void*)) = println!("item(1) = ", item)
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 2)
val ((*void*)) = println!("item(2) = ", item)
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 3)
val ((*void*)) = println!("item(3) = ", item)
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 4)
val ((*void*)) = println!("item(4) = ", item)
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 5)
val ((*void*)) = println!("item(5) = ", item)
//
val-
~Some_vt(item) =
 line_get_at(line, COMMA, 6)
val ((*void*)) = println!("item(6) = ", item)
//
#define N 10
//
val () =
println!("NYSE-daily(", N, ")")
val
theNYSE_daily =
NYSE_daily_get("../DATA/WRDS/NYSE-daily.csv")
val
theNYSE_daily_100 =
(stream_vt_takeLte(theNYSE_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("AMEX-daily(", N, ")")
val
theAMEX_daily =
AMEX_daily_get("../DATA/WRDS/AMEX-daily.csv")
val
theAMEX_daily_100 =
(stream_vt_takeLte(theAMEX_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("ARCO-daily(", N, ")")
val
theARCO_daily =
ARCO_daily_get("../DATA/WRDS/ARCO-daily.csv")
val
theARCO_daily_100 =
(stream_vt_takeLte(theARCO_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("SP500-daily(", N, ")")
val
theSP500_daily =
SP500_daily_get("../DATA/WRDS/SP500-daily.csv")
val
theSP500_daily_100 =
(stream_vt_takeLte(theSP500_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("NASDAQ-daily(", N, ")")
val
theNASDAQ_daily =
NASDAQ_daily_get("../DATA/WRDS/NASDAQ-daily.csv")
val
theNASDAQ_daily_100 =
(stream_vt_takeLte(theNASDAQ_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("NDX100-daily(", N, ")")
val
theNDX100_daily =
NDX100_daily_get("../DATA/Yahoo/NDX100-daily.csv")
val
theNDX100_daily_100 =
(stream_vt_takeLte(theNDX100_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("RUT2000-daily(", N, ")")
val
theRUT2000_daily =
RUT2000_daily_get("../DATA/Yahoo/RUT2000-daily.csv")
val
theRUT2000_daily_100 =
(stream_vt_takeLte(theRUT2000_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("QLD-daily(", N, ")")
val
theQLD_daily =
QLD_daily_get("../DATA/Yahoo/QLD-daily.csv")
val
theQLD_daily_100 =
(stream_vt_takeLte(theQLD_daily, N)).foreach()(lam x => println!(x))
//
val () =
println!("QID-daily(", N, ")")
val
theQID_daily =
QID_daily_get("../DATA/Yahoo/QID-daily.csv")
val
theQID_daily_100 =
(stream_vt_takeLte(theQID_daily, N)).foreach()(lam x => println!(x))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test01.dats] *)
