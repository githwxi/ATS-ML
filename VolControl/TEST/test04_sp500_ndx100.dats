(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
//
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#define ATSML_targetloc "./../.."
//
#staload "{$ATSML}/SATS/utils.sats"
#staload "{$ATSML}/DATS/utils.dats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
#staload "./../DATS/GetData.dats"
//
#staload "./../SATS/VolControl.sats"
//
#dynload "./../DATS/VolControl.dats"
//
(* ****** ****** *)

typedef retd = double
typedef vald = double

(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println! ("Hello from [test02]!")
//
val
theSP500_daily =
SP500_daily_get
  ("../DATA/WRDS/SP500-daily.csv")
//
val
theNDX100_daily =
NDX100_daily_get
  ("../DATA/Yahoo/NDX100-daily.csv")
val
theNDX100_daily =
stream_vt_datevald2retd(theNDX100_daily)
//
val xys =
stream2_vt_dateretd_sync
(theSP500_daily, theNDX100_daily)
val (xs, ys) = stream2list2_vt(xys)
//
val xs = list_vt2t(xs)
val ys = list_vt2t(ys)
//
val xs =
list_map_cloref<dateretd><retd>(xs, lam(x) => x.retd())
val ys =
list_map_cloref<dateretd><retd>(ys, lam(y) => y.retd())
//
val theSP500_daily = list_vt2t(xs)
val theNDX100_daily = list_vt2t(ys)
//
val () = println! "theSP500_daily:"
val () = pchanges_summarize(theSP500_daily) // SP500
val () = println! "theNDX100_daily:"
val () = pchanges_summarize(theNDX100_daily) // NDX100
//
(* ****** ****** *)
//
// fcontrol = selloff(20)
//
val l0 = 2.0
val s0 = 25.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theSP500_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls = VolControl_stream(theSP500_daily, l0, s0, k0, q0, 4, f0)
val () = println! "theSP500_daily:"
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
(* ****** ****** *)
//
// fcontrol = selloff(20)
//
val l0 = 2.0
val s0 = 25.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls = VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 4, f0)
val () = println! "theNDX100_daily:"
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
(* ****** ****** *)
//
// fcontrol = selloff(30)
// theNDX100_daily -> theSP500_daily
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () =
println!
("theNDX100_daily -> theSP500_daily")
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls =
VolControl2_stream
  (theNDX100_daily, theSP500_daily, l0, s0, k0, q0, 4, f0)
//
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
(* ****** ****** *)
//
// fcontrol = selloff(30)
// theSP500_daily -> theNDX100_daily
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () =
println!
("theSP500_daily -> theNDX100_daily")
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls =
VolControl2_stream
  (theSP500_daily, theNDX100_daily, l0, s0, k0, q0, 4, f0)
//
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test04_sp500_ndx100.dats] *)
