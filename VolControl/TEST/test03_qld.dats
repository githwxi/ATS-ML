(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
//
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
#staload "./../DATS/GetData.dats"
//
#staload "./../SATS/VolControl.sats"
//
#dynload "./../DATS/VolControl.dats"
//
(* ****** ****** *)

typedef retd = double
typedef vald = double

(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println! ("Hello from [test02]!")
//
val
theQLD_daily =
QLD_daily_get
  ("../DATA/Yahoo/QLD-daily.csv")
//
val
theQLD_daily =
stream_vt_datevald2retd(theQLD_daily)
val
theQLD_daily =
stream_vt_map_cloptr<dateretd><retd>
  (theQLD_daily, lam(x) => x.retd())
val
theQLD_daily =
list_vt2t(stream2list_vt(theQLD_daily))
//
val () = pchanges_summarize(theQLD_daily)
//
// fcontrol = 1.0
//
val l0 = 1.0
val s0 = 40.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = 1.0")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> l0
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = 2.0
//
val l0 = 2.0
val s0 = 40.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = 2.0")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> l0
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = reciprocal
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = reciprocal")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> 1/x
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = selloff(20)
//
val l0 = 1.0
val s0 = 40.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = selloff(25)
//
val l0 = 1.0
val s0 = 40.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(25)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.25 then 0.0 else l0)
//
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = selloff(30)
//
val l0 = 1.0
val s0 = 40.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(30)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.30 then 0.0 else l0)
//
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
//
// fcontrol = selloff(<30)
//
val l0 = 1.0
val s0 = 40.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(<30)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theQLD_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.30 then l0 else 0.0)
//
val volctrls = VolControl_stream(theQLD_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test03_qld.dats] *)
