(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
//
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#define ATSML_targetloc "./../.."
//
#staload "{$ATSML}/SATS/utils.sats"
#staload "{$ATSML}/DATS/utils.dats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
#staload "./../DATS/GetData.dats"
//
#staload "./../SATS/SharpeRatio.sats"
//
#dynload "./../DATS/SharpeRatio.dats"
//
(* ****** ****** *)

implement
main0() = () where
{
//
(*
val
theCount = ref<int>(0)
//
implement
{}(*tmp*)
fprint_datevald(out, x) =
let
val+DATEVALD(date, vald) = x
val n0 = theCount[]
val () = theCount[] := n0 + 1
//
in
  fprint!(out, "{ x: ", n0, ", ", "y: ", vald, " }")
end // end of [fprint_datevald]
*)
//
val
IRX =
("../DATA/Yahoo/IRX-daily.csv")
val
NDX100 =
("../DATA/Yahoo/NDX100-daily.csv")
//
val
dsrs =
SharpeRatio_stream_irx_ndx100
  (IRX, NDX100, 63(*13-weeks*))
//
val () =
dsrs.iforeach()(lam(i, x) => (if i > 0 then print ",\n"; println!(x)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test05_SR_irx_ndx100.dats] *)
