(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
//
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
#staload "./../DATS/GetData.dats"
//
#staload "./../SATS/VolControl.sats"
//
#dynload "./../DATS/VolControl.dats"
//
(* ****** ****** *)

typedef retd = double
typedef vald = double

(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println! ("Hello from [test02]!")
//
val
theNASDAQ_daily =
NASDAQ_daily_get
  ("../DATA/WRDS/NASDAQ-daily.csv")
//
val
theNASDAQ_daily =
stream_vt_map_cloptr<dateretd><retd>
  (theNASDAQ_daily, lam(x) => x.retd())
val
theNASDAQ_daily =
list_vt2t(stream2list_vt(theNASDAQ_daily))
//
val () = pchanges_summarize(theNASDAQ_daily)
//
// fcontrol = 1.0
//
val l0 = 1.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = 1.0")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNASDAQ_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> l0
val volctrls = VolControl_stream(theNASDAQ_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = 2.0
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = 2.0")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNASDAQ_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> l0
val volctrls = VolControl_stream(theNASDAQ_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = reciprocal
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = reciprocal")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNASDAQ_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> 1/x
val volctrls = VolControl_stream(theNASDAQ_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = selloff(10)
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(10)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNASDAQ_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.10 then 0.0 else l0)
//
val volctrls = VolControl_stream(theNASDAQ_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = selloff(20)
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNASDAQ_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls = VolControl_stream(theNASDAQ_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
// fcontrol = selloff(30)
//
val l0 = 2.0
val s0 = 20.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(30)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNASDAQ_daily >= k0+q0)
//
val f0 =
lam(x:double): double =<cloref> (if x >= 1.30 then 0.0 else l0)
//
val volctrls = VolControl_stream(theNASDAQ_daily, l0, s0, k0, q0, 0, f0)
val ((*void*)) = pchanges_summarize(list_vt2t(stream2list_vt(volctrls)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test02_nasdaq.dats] *)
