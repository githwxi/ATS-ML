(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
MATH =
"libats/libc/SATS/math.sats"
//
staload _ =
"libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#staload "./../DATS/GetData.dats"
//
(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println! ("Hello from [test00]!")
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test00.dats] *)
