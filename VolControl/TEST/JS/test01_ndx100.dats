(* ****** ****** *)
//
// For testing libatscc2js
//
(* ****** ****** *)
//
#define
LIBATSCC2JS_targetloc
"$PATSHOME/contrib\
/libatscc2js/ATS2-0.3.2"
//
(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME "test01_dynload"
//
#define ATS_STATIC_PREFIX "_test01_"
//
(* ****** ****** *)
//
#include
"{$LIBATSCC2JS}/staloadall.hats"
#staload
"{$LIBATSCC2JS}/SATS/print.sats"
//
(* ****** ****** *)

#staload "./../../SATS/GetData.sats"

(* ****** ****** *)

local
#include "./../../DATS_JS/GetData.dats"
in (*nothing*) end
local
#include "./../../DATS_JS/NDX100-daily.dats"
in (*nothing*) end

(* ****** ****** *)
//
implement
{}(*tmp*)
print_dateretd(x) =
let
val+
DATERETD(date, retd) = x in print!(date, ": ", retd)
end // end of [let]
//
implement
{}(*tmp*)
print_datevald(x) =
let
val+
DATEVALD(date, vald) = x in print!(date, ": ", vald)
end // end of [let]
//
(* ****** ****** *)
//
val
theNDX100_daily =
NDX100_daily_get("theNDX100_daily_csv")
//
(* ****** ****** *)

val () =
theNDX100_daily.foreach()(lam x => println!(x))

(* ****** ****** *)

%{^
//
// file inclusion
//
var fs = require('fs');
eval(fs.readFileSync('./../../../ASSET/JS/libatscc2js_all.js').toString());
eval(fs.readFileSync('./../../../ASSET/JS/CATS/PRINT/print_store_cats.js').toString());
//
%} // end of [%{^]

(* ****** ****** *)

%{$
test01_dynload();
process.stdout.write(ats2jspre_the_print_store_join());
%} // end of [%{$]

(* ****** ****** *)

(* end of [test01.dats] *)
