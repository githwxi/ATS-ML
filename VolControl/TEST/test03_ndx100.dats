(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
//
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#staload "./../SATS/GetData.sats"
#staload "./../DATS/GetData.dats"
//
#staload "./../SATS/VolControl.sats"
//
#dynload "./../DATS/VolControl.dats"
//
(* ****** ****** *)

typedef retd = double
typedef vald = double

(* ****** ****** *)

fun
VolControl_plot_return
  (rs: List(retd)) = let
//
fun
loop
( rs: List(retd)
, i0: int, ret: double): void =
(
case+ rs of
| list_nil() => ()
| list_cons(r, rs) => let
    val ret = ret * (1+r)
  in
    print(",\n");
    println!
    ("{ x: ", i0, ", ", "y: ", ret, " }");
    loop(rs, i0+1, ret);
  end // end of [list_cons]
)
//
in
//
println!("{ x: 0, y: 1.0 }"); loop(rs, 1, 1.0)
//
end // [VolControl_plot_return]

(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println! ("Hello from [test02]!")
//
val
theNDX100_daily =
NDX100_daily_get
  ("../DATA/Yahoo/NDX100-daily.csv")
//
val
theNDX100_daily =
stream_vt_datevald2retd(theNDX100_daily)
val
theNDX100_daily =
stream_vt_map_cloptr<dateretd><retd>
  (theNDX100_daily, lam(x) => x.retd())
val
theNDX100_daily =
list_vt2t(stream2list_vt(theNDX100_daily))
//
val () = pchanges_summarize(theNDX100_daily)
//
// fcontrol = 1.0
//
val l0 = 1.0
val s0 = 26.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = 1.0")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> l0
//
val volctrls =
VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 0, f0)
val volctrls =
list_vt2t(stream2list_vt(volctrls))
//
val ((*void*)) = pchanges_summarize(volctrls)
val ((*void*)) = VolControl_plot_return(volctrls)
//
// fcontrol = 2.0
//
val l0 = 2.0
val s0 = 26.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = 2.0")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> l0
//
val volctrls =
VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 0, f0)
val volctrls =
list_vt2t(stream2list_vt(volctrls))
//
val ((*void*)) = pchanges_summarize(volctrls)
val ((*void*)) = VolControl_plot_return(volctrls)
//
// fcontrol = reciprocal
//
val l0 = 2.0
val s0 = 26.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = reciprocal")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 = lam(x:double): double =<cloref> 1/x
//
val volctrls =
VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 0, f0)
val volctrls =
list_vt2t(stream2list_vt(volctrls))
//
val ((*void*)) = pchanges_summarize(volctrls)
val ((*void*)) = VolControl_plot_return(volctrls)
//
// fcontrol = selloff(10)
//
val l0 = 2.0
val s0 = 26.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(10)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 =
lam(x:double): double
  =<cloref> (if x >= 1.10 then 0.0 else l0)
//
val volctrls =
VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 0, f0)
val volctrls =
list_vt2t(stream2list_vt(volctrls))
//
val ((*void*)) = pchanges_summarize(volctrls)
val ((*void*)) = VolControl_plot_return(volctrls)
//
// fcontrol = selloff(20)
//
val l0 = 2.0
val s0 = 26.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(20)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 =
lam(x:double): double
  =<cloref> (if x >= 1.20 then 0.0 else l0)
//
val volctrls =
VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 0, f0)
val volctrls =
list_vt2t(stream2list_vt(volctrls))
//
val ((*void*)) = pchanges_summarize(volctrls)
val ((*void*)) = VolControl_plot_return(volctrls)
//
// fcontrol = selloff(30)
//
val l0 = 2.0
val s0 = 26.0
val k0 = 21 and q0 = 0
//
val () = println! ("lever0 = ", l0)
val () = println! ("sigma0 = ", s0)
val () = println! ("winoff = ", q0)
val () = println! ("winlen = ", k0)
val () = println! ("fcontrol = selloff(30)")
//
val s0 = s0/100/$M.sqrt(252.0)
//
val () = assertloc(theNDX100_daily >= k0+q0)
//
val f0 =
lam (x:double): double
  =<cloref> (if x >= 1.30 then 0.0 else l0)
//
val volctrls =
VolControl_stream(theNDX100_daily, l0, s0, k0, q0, 0, f0)
val volctrls =
list_vt2t(stream2list_vt(volctrls))
//
val ((*void*)) = pchanges_summarize(volctrls)
val ((*void*)) = VolControl_plot_return(volctrls)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test03_ndx100.dats] *)
