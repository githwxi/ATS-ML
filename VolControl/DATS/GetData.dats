(* ****** ****** *)
(*
** For getting index data
** For getting stock data
*)
(* ****** ****** *)
//
#define ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#staload
UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

staload "./../SATS/GetData.sats"

(* ****** ****** *)
//
#define COMMA ','
//
(* ****** ****** *)
//
extern
fun
memcpy
( d0: ptr
, s0: ptr
, n0: size_t
) :<!wrt> ptr = "mac#atspre_string_memcpy"
// end of [memcpy]
//
(* ****** ****** *)
//
extern
fun
line_get_at
(
line: string,
delim: charNZ, i0: intGte(0)
) : Option_vt(string)
//
implement
line_get_at
(
  line, delim, i0
) = let
//
#define CNUL '\000'
//
fun
loop1
(
p0: ptr
) : ptr = let
  val
  chr =
  $UN.ptr0_get<char>(p0)
in
  if
  (chr=delim)
  then p0 else
  (
    if iseqz(chr)
      then p0 else loop1(succ<char>(p0))
    // end of [if]
  ) (* end of [if] *)
end // end of [loop1]
//
fun
loop2
(
p0: ptr, i: int
) : ptr =
(
if
(i <= 0)
then p0
else let
  val
  chr =
  $UN.ptr0_get<char>(p0)
in
  if chr = delim
    then loop2(succ<char>(p0), i-1)
    else
    (
      if isneqz(chr)
        then loop2(succ<char>(p0), i) else the_null_ptr
      // end of [if]
    )
  // end of [if]
end // end of [else]
)
//
val p0 =
  string2ptr(line)
//
val p1 = loop2(p0, i0)
//
in
//
if
isneqz(p1)
then let
  val p2 = loop1(p1)
  val ln =
    ptr0_diff<char>(p2, p1)
  // end of [val]
  val ln = $UN.cast{Size}(ln)
  val (pf, pfgc | p_res) = malloc_gc(ln)
//
  val p_res =
    memcpy(p_res, p1, ln)
  val ((*void*)) =
    $UN.ptr0_set_at<char>(p_res, ln, CNUL)
//
in
  Some_vt($UN.castvwtp0{string}((pf, pfgc | p_res)))
end // end of [then]
else None_vt((*void*))
//
end // end of [line_get_at]

(* ****** ****** *)
//
extern
fun
lines_get_at
(
lines: stream_vt(string),
delim: charNZ, i0: intGte(0)
) : stream_vt(string)
//
implement
lines_get_at
(lines, delim, i0) =
stream_vt_mapopt_cloptr
(lines, lam(line) => line_get_at(line, delim, i0))
//
(* ****** ****** *)
//
implement
{}(*tmp*)
print_dateretd
  (x) = fprint_dateretd(stdout_ref, x)
implement
{}(*tmp*)
prerr_dateretd
  (x) = fprint_dateretd(stderr_ref, x)
//
implement
{}(*tmp*)
print_datevald
  (x) = fprint_datevald(stdout_ref, x)
implement
{}(*tmp*)
prerr_datevald
  (x) = fprint_datevald(stderr_ref, x)
//
(* ****** ****** *)
//
implement
{}(*tmp*)
fprint_dateretd(out, x) =
let val+DATERETD(date, retd) = x in fprint!(out, date, ": ", retd) end
implement
{}(*tmp*)
fprint_datevald(out, x) =
let val+DATEVALD(date, vald) = x in fprint!(out, date, ": ", vald) end
//
(* ****** ****** *)
//
implement
dateretd_get_date(x) =
let val+DATERETD(date, _) = x in date end
implement
dateretd_get_retd(x) =
let val+DATERETD(_, retd) = x in retd end
//
implement
datevald_get_date(x) =
let val+DATEVALD(date, _) = x in date end
implement
datevald_get_vald(x) =
let val+DATEVALD(_, vald) = x in vald end
//
(* ****** ****** *)

implement
stream_vt_datevald2retd
  (xs) = let
//
fun
auxmain
(
v0: double,
xs: stream_vt(datevald)
) : stream_vt(dateretd) = $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
   stream_vt_nil()
| ~stream_vt_cons(x1, xs) => let
    val v1 = x1.vald()
    val rd =
      DATERETD(x1.date(), v1/v0-1.0)
    // end of [val]
  in
    stream_vt_cons(rd, auxmain(v1, xs))
  end
) , lazy_vt_free(xs)
)
//
in
  case+ !xs of
  | ~stream_vt_nil() => stream_vt_make_nil()
  | ~stream_vt_cons(x0, xs) => auxmain(x0.vald(), xs)
end // end of [stream_vt_datevald2retd]

(* ****** ****** *)

implement
stream2_vt_dateretd_sync
  (xs, ys) = let
//
fun
auxmain
(
xs: stream_vt(dateretd)
,
ys: stream_vt(dateretd)
) : stream_vt(dateretd2) = $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
  (~ys; stream_vt_nil())
| ~stream_vt_cons(x0, xs) =>
  (
    case+ !ys of
    | ~stream_vt_nil() =>
      (~xs; stream_vt_nil())
    | ~stream_vt_cons(y0, ys) => let
        val dx = x0.date()
        val dy = y0.date()
        val sgn = compare(dx, dy)
      in
        ifcase
        | sgn > 0 => !(auxmainx(x0, xs, ys))
        | sgn < 0 => !(auxmainy(y0, xs, ys))
        | _ (*else*) => stream_vt_cons((x0, y0), auxmain(xs, ys))
      end // end of [stream_vt_cons]

  )
) , (lazy_vt_free(xs); lazy_vt_free(ys))
)
//
and
auxmainx
(
x0: dateretd
,
xs: stream_vt(dateretd)
,
ys: stream_vt(dateretd)
) : stream_vt(dateretd2) = $ldelay
(
(
case+ !ys of
| ~stream_vt_nil() =>
  (~xs; stream_vt_nil())
| ~stream_vt_cons(y0, ys) => let
    val dx = x0.date()
    val dy = y0.date()
    val sgn = compare(dx, dy)
  in
    ifcase
    | sgn > 0 => !(auxmainx(x0, xs, ys))
    | sgn < 0 => !(auxmainy(y0, xs, ys))
    | _ (*else*) => stream_vt_cons((x0, y0), auxmain(xs, ys))
  end // end of [stream_vt_cons]
) , (lazy_vt_free(xs); lazy_vt_free(ys))
)
//
and
auxmainy
(
y0: dateretd
,
xs: stream_vt(dateretd)
,
ys: stream_vt(dateretd)
) : stream_vt(dateretd2) = $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
  (~ys; stream_vt_nil())
| ~stream_vt_cons(x0, xs) => let
    val dx = x0.date()
    val dy = y0.date()
    val sgn = compare(dx, dy)
  in
    ifcase
    | sgn > 0 => !(auxmainx(x0, xs, ys))
    | sgn < 0 => !(auxmainy(y0, xs, ys))
    | _ (*else*) => stream_vt_cons((x0, y0), auxmain(xs, ys))
  end // end of [stream_vt_cons]
) , (lazy_vt_free(xs); lazy_vt_free(ys))
)
//
in
  auxmain(xs, ys)
end // end of [stream2_vt_dateretd_sync]

(* ****** ****** *)

implement
IRX_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    val opt2 = line_get_at(line, COMMA, 1)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(retd) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(retd)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [IRX_daily_get]

(* ****** ****** *)

implement
NYSE_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(dateretd) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    val opt2 = line_get_at(line, COMMA, 1)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(retd) =>
       Some_vt(DATERETD(g0string2int(date), g0string2float(retd)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = dateretd
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [NYSE_daily_get]

(* ****** ****** *)

implement
AMEX_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(dateretd) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    val opt2 = line_get_at(line, COMMA, 1)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(retd) =>
       Some_vt(DATERETD(g0string2int(date), g0string2float(retd)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = dateretd
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [AMEX_daily_get]

(* ****** ****** *)

implement
ARCO_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(dateretd) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    val opt2 = line_get_at(line, COMMA, 1)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(retd) =>
       Some_vt(DATERETD(g0string2int(date), g0string2float(retd)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = dateretd
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [ARCO_daily_get]

(* ****** ****** *)

implement
SP500_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(dateretd) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    val opt2 = line_get_at(line, COMMA, 1)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(retd) =>
       Some_vt(DATERETD(g0string2int(date), g0string2float(retd)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = dateretd
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [SP500_daily_get]

(* ****** ****** *)

implement
NASDAQ_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(dateretd) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    val opt2 = line_get_at(line, COMMA, 1)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(retd) =>
       Some_vt(DATERETD(g0string2int(date), g0string2float(retd)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = dateretd
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [NASDAQ_daily_get]

(* ****** ****** *)

implement
NDX100_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [NDX100_daily_get]

(* ****** ****** *)

implement
RUT2000_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [RUT2000_daily_get]

(* ****** ****** *)

implement
QLD_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [QLD_daily_get]

(* ****** ****** *)

implement
QID_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [QID_daily_get]

(* ****** ****** *)

implement
TQQQ_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [TQQQ_daily_get]

(* ****** ****** *)

implement
SQQQ_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [SQQQ_daily_get]

(* ****** ****** *)

implement
SPUU_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [SPUU_daily_get]

(* ****** ****** *)

implement
SPXU_daily_get
  (fname) = let
//
val
opt =
fileref_open_opt
(fname, file_mode_r)
val-~Some_vt(filr) = opt
//
val _(*line*) =
fileref_get_line_string(filr)
//
val filp =
$UN.castvwtp0{FILEptr1}(filr)
val lines =
  streamize_fileptr_line(filp)
//
fun
fopr_line
(
line: string
) : Option_vt(datevald) = let
//
val opt =
line_get_at(line, COMMA, 0)
//
in
//
case+ opt of
| ~None_vt() =>
   None_vt()
| ~Some_vt(date) => let
    // close: 4
    // adjclose: 5
    val opt2 = line_get_at(line, COMMA, 5)
  in
    case+ opt2 of
    | ~None_vt() =>
       None_vt()
    | ~Some_vt(vald) =>
       Some_vt(DATEVALD(g0string2int(date), g0string2float(vald)))
  end // end of [Some_vt]
//
end // end of [fopr_line]
//
typedef a = string and b = datevald
//
in
  stream_vt_mapopt_cloptr<a><b>(lines, lam(line) => fopr_line(line))
end // end of [SPXU_daily_get]

(* ****** ****** *)

(* end of [GetData.dats] *)
