(* ****** ****** *)
(*
** For simulating
** volatility control
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "VolControl"
//
#define
ATS_EXTERN_PREFIX "_VolControl_"
#define
ATS_STATIC_PREFIX "_VolControl_"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#define
ATSML_targetloc "./../.."
//
#include
"{$ATSML}/mylibies.hats"
//
#staload $UTILS // open namespace
#staload $STATS // open namespace
//
(* ****** ****** *)

#staload "./../SATS/GetData.sats"
#staload "./../SATS/VolControl.sats"

(* ****** ****** *)
//
implement
Kappa_stream
(xs, l0, s0, k0, f0) = let
//
val ss =
list_rolling_stdev(xs, k0)
//
(*
val () =
println!
("Kappa_stream: |xs| = ", length(xs))
*)
//
in
//
stream_vt_map_cloptr
  (ss, lam(s1) => min(f0(s1/s0), l0))
//
end // end of [Kappa_stream]
//
(* ****** ****** *)
//
implement
VolControl_stream
(
rs, l0, s0,
k0, q0, step, fctrl
) =
(
//
VolControl2_stream
  (rs, rs, l0, s0, k0, q0, step, fctrl)
//
) (* VolControl_stream *)
//
(* ****** ****** *)
//
implement
VolControl2_stream
(
ks, rs, l0, s0,
k0, q0, step, fctrl
) = let
//
val
kappas =
Kappa_stream
  (ks, l0, s0, k0, fctrl)
val
kappas =
(
if
(step <= 0)
then
kappas
else
stream_vt_stepize(kappas, step)
) : stream_vt(double)
//
fun
auxmain
(
rs:
List(double),
kappas:
stream_vt(double)
) :
stream_vt(double) = $ldelay
(
case+ rs of
| list_nil() =>
  (~kappas;
   stream_vt_nil()
  ) (* list_nil *)
| list_cons(r, rs) =>
  (
    case+ !kappas of
    | ~stream_vt_nil() =>
        stream_vt_nil()
    | ~stream_vt_cons(kappa, kappas) =>
      (
        stream_vt_cons(kappa * r, auxmain(rs, kappas))
      ) (* end of [stream_vt_cons] *)
  ) (* list_cons *)
, lazy_vt_free(kappas)
)
//
val kq0 = k0+q0
//
val () = assertloc(rs >= kq0)
//
(*
val () =
println!
  ("VolControl2_stream: kq0 = ", kq0)
*)
//
in
  auxmain(list_drop(rs, kq0), kappas)
end // end of [VolControl2_stream]
//
(* ****** ****** *)
//
implement
kappas_summarize
  (ks) = () where
{
//
(*
fun
auxlst
(ks: List(double)): void =
(
case+ ks of
| list_nil
    () => ()
  // list_nil
| list_cons
    (k, ks) =>
    auxlst(ks) where
  {
    val () =
    (
    echo("kappa = ", k); echoln()
    )
  } (* end of [list_cons] *)
)
val ((*void*)) = auxlst(ks)
*)
//
#define EPSILON 0.01
//
fnx
auxlst0
(ks: List(double)): void =
(
case+ ks of
| list_nil() => ()
| list_cons(k0, ks) => auxlst1(ks, k0, 1)
)
//
and
auxlst1
(
ks: List(double), k0: double, nk: int
) : void =
(
case+ ks of
| list_nil() =>
  (
    println!("kappa = ", k0, " => ", nk);
  )
| list_cons(k1, ks) =>
  if abs(k1-k0) < EPSILON
    then
    (
      auxlst1(ks, k0, nk+1)
    )
    else
    (
      println!("kappa = ", k0, " => ", nk); auxlst1(ks, k1, 1)
    )
  // end of [if] // end of [list_cons]
)
//
val () = auxlst0(ks)
//
} (* end of [kappas_summarize] *)
//
(* ****** ****** *)
//
implement
pchanges_summarize
  (xs) =
{
//
val
nxs = length(xs)
//
val () = let
  val size = nxs
in
//
println!
("pchanges_summarize: size=", size);
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val max = list_max(xs)
in
//
println!("pchanges_summarize: max=", max);
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val min = list_min(xs)
in
//
println!("pchanges_summarize: min=", min);
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val mean = list_mean(xs)
in
//
println!
  ("pchanges_summarize: mean=", mean);
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val stdev = list_stdev(xs)
  val stdev2 = $M.sqrt(252.0) * stdev
in
//
println!
  ("pchanges_summarize: stdev=", stdev);
println!
  ("pchanges_summarize: stdev(annual)=", stdev2);
//
end // end of [if] // end of [val]
//
typedef dbl = double
//
val
totret =
list_foldleft_cloref<dbl><dbl>
  (xs, 1.0, lam(res, x) => res*(1+x))
//
val () = let
//
val totret = totret
//
in
//
println!
  ("pchanges_summarize: totret=", totret)
//
end // end of [val]
//
val () = let
//
val
avgret =
$M.exp($M.log(totret)/(nxs))-1
val
avgret2 = $M.exp(252*$M.log(totret)/(nxs))-1
//
in
//
println!
  ("pchanges_summarize: avgret=", avgret);
println!
  ("pchanges_summarize: avgret(annual)=", avgret2);
//
end // end of [val]
//
val vs = aux(xs, 1.0) where
{
fun
aux
(
xs: List(double), v0: double
) : stream_vt(double) = $ldelay
(
case+ xs of
| list_nil() =>
  stream_vt_nil()
| list_cons(x, xs) =>
  stream_vt_cons(v0, aux(xs, v0*(1+x)))
) (* end of [aux] *)
}
//
val vs =
list_vt2t(stream2list_vt(vs))
val MDD = list_maxdrawdown(vs)
//
val () = println!("pchanges_summarize: MDD=", MDD);
//
} (* end of [pchanges_summarize] *)

(* ****** ****** *)

(* end of [VolControl.dats] *)
