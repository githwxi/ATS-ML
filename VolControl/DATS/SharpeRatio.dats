(* ****** ****** *)
(*
** For simulating
** volatility control
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "SharpeRatio"
//
#define
ATS_EXTERN_PREFIX "_SharpeRatio_"
#define
ATS_STATIC_PREFIX "_SharpeRatio_"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
M = "libats/libc/SATS/math.sats"
staload
_ = "libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#define
ATSML_targetloc
"./../.."
//
#include
"{$ATSML}/mylibies.hats"
//
#staload $UTILS // open namespace
#staload $STATS // open namespace
//
(* ****** ****** *)

#staload
"./../SATS/GetData.sats"

(* ****** ****** *)
//
#staload
"./../SATS/SharpeRatio.sats"
//
(* ****** ****** *)
//
implement
gdata_eval<dateretd>(x) = x.retd()
implement
gdata_eval<datevald>(x) = x.vald()
//
(* ****** ****** *)

implement
SharpeRatio
(t0, rs, nr) = let
//
macdef exp = $M.exp
macdef log = $M.log
macdef pow = $M.pow
macdef sqrt = $M.sqrt
//
val
stdev =
glistpre_stdev(rs, nr)
val
accret =
glistpre_accret(rs, nr)
//
val
riskfree =
exp(nr*log(1+t0/100)/252)-1
//
(*
val () = println! ("t0 = ", t0)
val () = println! ("accret = ", accret)
val () = println! ("riskfree = ", riskfree)
*)
//
in
  (accret - riskfree) / (stdev * sqrt(1.0*nr))
end // end of [SharpeRatio]

(* ****** ****** *)

local

fun
droplst_dts_gte
(
  dts: List(datevald), d0: int
) : List(datevald) =
(
case+ dts of
| list_nil() =>
  list_nil()
| list_cons(dr1, dts2) =>
    if dr1.date() >= d0
      then dts else droplst_dts_gte(dts2, d0)
  // end of [list_cons]
)
fun
droplst_drs_gte
(
  drs: List(dateretd), d0: int
) : List(dateretd) =
(
case+ drs of
| list_nil() =>
  list_nil()
| list_cons(dr1, drs2) =>
    if dr1.date() >= d0
      then drs else droplst_drs_gte(drs2, d0)
  // end of [list_cons]
)

(* ****** ****** *)

fun
countlst_drs_lt
(
  drs: List(dateretd), d0: int
) : intGte(0) = let
//
fun
loop (
  drs: List(dateretd), res: intGte(0)
) : intGte(0) =
(
case+ drs of
| list_nil() => res
| list_cons(dr, drs) =>
  (
    if dr.date() < d0 then loop(drs, res+1) else res 
  )
)
//
in
  loop(drs, 0)
end // end of [countlst_drs_lt]
fun
countlst_dts_lt
(
  dts: List(datevald), d0: int
) : intGte(0) = let
//
fun
loop (
  dts: List(datevald), res: intGte(0)
) : intGte(0) =
(
case+ dts of
| list_nil() => res
| list_cons(dt, dts) =>
  (
    if dt.date() < d0 then loop(dts, res+1) else res 
  )
)
//
in
  loop(dts, 0)
end // end of [countlst_dts_lt]

(* ****** ****** *)

fun
auxmain
(
  dts1: List(datevald),
  drs1: List(dateretd),
  drs2: List(dateretd), wd: int
) : stream_vt(datevald) = $ldelay
(
//
case+ drs2 of
| list_nil
    ((*void*)) =>
    stream_vt_nil()
| list_cons
    (dr2, drs2) => let
    val d0 = dr2.date()
    val nt =
      countlst_dts_lt(dts1, d0)
    val nr =
      countlst_drs_lt(drs1, d0)
    val () = assertloc(nt >= 1)
    val () = assertloc(nr >= 1)
    val () = assertloc(dts1 >= 1)
    val () = assertloc(drs1 >= nr)
    val t0 = glistpre_mean(dts1, nt)
    val sr = SharpeRatio(t0, drs1, nr)
    val dts1 =
    (
      if (nt >= wd)
        then list_tail(dts1) else dts1
    ) : List(datevald)
    val drs1 =
    (
      if (nr >= wd)
        then list_tail(drs1) else drs1
    ) : List(dateretd)
    val dsr = DATEVALD(dr2.date(), sr)
  in
    stream_vt_cons(dsr, auxmain(dts1, drs1, drs2, wd))
  end // end of [list_cons]
//
) (* end of [auxmain] *)

in (* in-of-local *)

implement
SharpeRatio_stream
(
  dts, drs, width
) = let
//
val-list_cons(dt, _) = dts
val-list_cons(dr, _) = drs
//
val d0 = max(dr.date(), dt.date())
//
val dts1 = droplst_dts_gte(dts, d0)
val drs1 = droplst_drs_gte(drs, d0)
//
val drs2 = list_drop_exn(drs1, width)
//
in
  auxmain(dts1, drs1, drs2, width)
end // end of [SharpeRatio_stream]

end // end of [local]

(* ****** ****** *)

implement
SharpeRatio_stream_irx_nyse
  (dts, drs, width) = let
//
val dts =
list_vt2t
(
stream2list_vt(IRX_daily_get(dts))
)
val drs =
list_vt2t
(
stream2list_vt(NYSE_daily_get(drs))
)
//
in
  SharpeRatio_stream(dts, drs, width)
end // end of [SharpeRatio_stream_irx_nyse]

(* ****** ****** *)

implement
SharpeRatio_stream_irx_sp500
  (dts, drs, width) = let
//
val dts =
list_vt2t
(
stream2list_vt(IRX_daily_get(dts))
)
val drs =
list_vt2t
(
stream2list_vt(SP500_daily_get(drs))
)
//
in
  SharpeRatio_stream(dts, drs, width)
end // end of [SharpeRatio_stream_irx_sp500]

(* ****** ****** *)

implement
SharpeRatio_stream_irx_ndx100
  (dts, drs, width) = let
//
val dts =
list_vt2t
(
stream2list_vt(IRX_daily_get(dts))
)
//
val drs =
list_vt2t
(
stream2list_vt
(stream_vt_datevald2retd(NDX100_daily_get(drs)))
)
//
in
  SharpeRatio_stream(dts, drs, width)
end // end of [SharpeRatio_stream_irx_ndx100]

(* ****** ****** *)

(* end of [SharpeRatio.dats] *)
