(* ****** ****** *)
(*
** SharpeRatio control
*)
(* ****** ****** *)
//
#staload "./GetData.sats"
//
(* ****** ****** *)
//
fun
SharpeRatio
{nr:int | nr >= 1}
( t0: double
, rs: listGte(dateretd, nr), nr: int(nr)): double
//
(* ****** ****** *)
//
fun
SharpeRatio_stream
(
ts: List(datevald), rs: List(dateretd), width: intGte(1)
) : stream_vt(datevald)
//
(* ****** ****** *)
//
fun
SharpeRatio_stream_irx_nyse
  (dts: string, drs: string, width: intGte(1)) : stream_vt(datevald)
//
fun
SharpeRatio_stream_irx_sp500
  (dts: string, drs: string, width: intGte(1)) : stream_vt(datevald)
//
fun
SharpeRatio_stream_irx_ndx100
  (dts: string, drs: string, width: intGte(1)) : stream_vt(datevald)
//
(* ****** ****** *)

(* end of [SharpeRatio.dats] *)
