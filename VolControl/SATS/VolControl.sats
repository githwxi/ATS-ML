(* ****** ****** *)
(*
** Volatility control
*)
(* ****** ****** *)
//
#staload
"libats/ML/SATS/basis.sats"
//
(* ****** ****** *)

staload "./GetData.sats"

(* ****** ****** *)
//
// k0: width of the rolling window
// l0: leverage cap;
// s0=sigma0: base volatility
// f0: for controlling volatility
//
// Please see equation (1) in the paper.
//
fun
Kappa_stream
{k:int | k >= 1}
{n:int | n >= k}
(
xs: list(double, n),
l0: double, s0: double,
k0: int(k), f0: cfun(double, double)
) : stream_vt(double)
//
(* ****** ****** *)
//
fun
VolControl_stream
{k,q,n:int
|k >= 1;
 k+q >= 0; n >= k}
(
rs: list(double, n),
l0: double, s0: double,
k0: int(k), q0: int(q),
//
step: intGte(0),
//
fctrl: cfun(double, double)
//
) : stream_vt(double)
//
fun
VolControl2_stream
{k,q,n:int
|k >= 1;
 k+q >= 0; n >= k}
(
ks: list(double, n),
rs: list(double, n),
l0: double, s0: double,
k0: int(k), q0: int(q),
//
step: intGte(0),
//
fctrl: cfun(double, double)
//
) : stream_vt(double)
//
(* ****** ****** *)
//
fun
kappas_summarize(xs: List(double)): void
fun
pchanges_summarize(xs: List(double)): void
//
(* ****** ****** *)

(* end of [VolControl.dats] *)
