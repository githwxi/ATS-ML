(* ****** ****** *)
(*
** For getting index data
** For getting stock data
*)
(* ****** ****** *)
//
datatype
dateretd =
DATERETD of (int, double)
datatype
datevald =
DATEVALD of (int, double)
//
(* ****** ****** *)
//
fun{}
print_dateretd : print_type(dateretd)
fun{}
prerr_dateretd : prerr_type(dateretd)
fun{}
fprint_dateretd : fprint_type(dateretd)
//
overload print with print_dateretd
overload prerr with prerr_dateretd
overload fprint with fprint_dateretd
//
fun{}
print_datevald : print_type(datevald)
fun{}
prerr_datevald : prerr_type(datevald)
fun{}
fprint_datevald : fprint_type(datevald)
//
overload print with print_datevald
overload prerr with prerr_datevald
overload fprint with fprint_datevald
//
(* ****** ****** *)
//
fun
dateretd_get_date(dateretd):<> int
fun
dateretd_get_retd(dateretd):<> double
//
fun
datevald_get_date(datevald):<> int
fun
datevald_get_vald(datevald):<> double
//
overload .date with dateretd_get_date
overload .retd with dateretd_get_retd
overload .date with datevald_get_date
overload .vald with datevald_get_vald
//
(* ****** ****** *)
//
fun
stream_vt_datevald2retd
(xs: stream_vt(datevald)): stream_vt(dateretd)
//
(* ****** ****** *)
//
typedef
dateretd2 = (dateretd, dateretd)
//
fun
stream2_vt_dateretd_sync
( xs: stream_vt(dateretd)
, ys: stream_vt(dateretd)): stream_vt(dateretd2)
//
(* ****** ****** *)
//
fun
IRX_daily_get
  (filename: string): stream_vt(datevald)
//
(* ****** ****** *)
//
fun
NYSE_daily_get
  (filename: string): stream_vt(dateretd)
fun
AMEX_daily_get
  (filename: string): stream_vt(dateretd)
fun
ARCO_daily_get
  (filename: string): stream_vt(dateretd)
fun
SP500_daily_get
  (filename: string): stream_vt(dateretd)
fun
NASDAQ_daily_get
  (filename: string): stream_vt(dateretd)
//
(* ****** ****** *)
//
fun
NDX100_daily_get
  (filename: string): stream_vt(datevald)
fun
RUT2000_daily_get
  (filename: string): stream_vt(datevald)
//
(* ****** ****** *)
//
// Some selected ETF's ...
//
(* ****** ****** *)
//
fun
QLD_daily_get // NDX100 * (2)
  (filename: string): stream_vt(datevald)
fun
QID_daily_get // NDX100 * (-2)
  (filename: string): stream_vt(datevald)
//
(* ****** ****** *)
//
fun
TQQQ_daily_get // NDX100 * (3)
  (filename: string): stream_vt(datevald)
fun
SQQQ_daily_get // NDX100 * (-3)
  (filename: string): stream_vt(datevald)
//
(* ****** ****** *)
//
fun
SPUU_daily_get // SP500 * (2)
  (filename: string): stream_vt(datevald)
fun
SPXU_daily_get // SP500 * (-2)
  (filename: string): stream_vt(datevald)
//
(* ****** ****** *)

(* end of [GetData.sats] *)
