(* ****** ****** *)
//
// HX-2017-08-01
// For downstream staloading
//
(* ****** ****** *)
//
// For various statistical funs
//
(* ****** ****** *)
//
#staload
UTILS = "./SATS/utils.sats"
#staload
_(*UTILS*) = "./DATS/utils.dats"
//
(* ****** ****** *)
//
#staload
STATS = "./SATS/stats.sats"
#staload
_(*STATS*) = "./DATS/stats_tmp.dats"
//
(* ****** ****** *)

(* end of [mylibies.hats] *)
