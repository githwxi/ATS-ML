(* ****** ****** *)
(*
** For various utilities
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "ATS-ML"
//
(* ****** ****** *)
//
#define ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
#staload UN = $UNSAFE
//
(* ****** ****** *)
//
#staload "./../SATS/utils.sats"
//
(* ****** ****** *)
//
implement
{a,b}
stream_vt_zip
  (xs, ys) =
  auxmain(xs, ys) where
{
//
fun
auxmain:
$d2ctype(stream_vt_zip<a,b>) =
lam(xs, ys) => $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
  (~ys; stream_vt_nil())
| ~stream_vt_cons(x0, xs) =>
  (
  case+ !ys of
  | ~stream_vt_nil() =>
    (~xs; stream_vt_nil())
  | ~stream_vt_cons(y0, ys) =>
    (stream_vt_cons((x0, y0), auxmain(xs, ys)))
  )
) , (lazy_vt_free(xs); lazy_vt_free(ys))
)
//
} (* end of [stream_vt_zip] *)
//
(* ****** ****** *)
//
implement
{a,b}
stream2list2_vt
  (xys) = let
//
vtypedef ab = @(a, b)
//
fun
loop
( xys: stream_vt(ab)
, xs0: &ptr? >> list_vt(a, n)
, ys0: &ptr? >> list_vt(b, n)): #[n:nat] void =
(
case+ !xys of
| ~stream_vt_nil() =>
  (
    xs0 := list_vt_nil();
    ys0 := list_vt_nil();
  )
| ~stream_vt_cons
    ((x, y), xys) =>
  {
    val () =
    ( xs0 :=
      list_vt_cons{a}{0}(x, _)
    )
    val () =
    ( ys0 :=
      list_vt_cons{b}{0}(y, _)
    )
    val+list_vt_cons(_, xs1) = xs0
    val+list_vt_cons(_, ys1) = ys0
    val ((*void*)) = loop(xys, xs1, ys1)
    val ((*folded*)) = fold@(xs0) 
    val ((*folded*)) = fold@(ys0) 
  }
)
//
var xs0: ptr and ys0: ptr
//
in
  let val () = loop(xys, xs0, ys0) in (xs0, ys0) end
end // end of [stream2list2_vt]
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
stream_vt_stepize
  (xs, step0) =
  aux0(xs) where
{
//
fun
aux0
(
xs: stream_vt(a)
) : stream_vt(a) = $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
   stream_vt_nil()
| ~stream_vt_cons(x0, xs) =>
   stream_vt_cons(x0, aux1(xs, x0, step0))
), lazy_vt_free(xs)
)
and
aux1
(
xs: stream_vt(a),
x0: a, step: int
) : stream_vt(a) = $ldelay
(
(
case+ !xs of
| ~stream_vt_nil() =>
   stream_vt_nil()
| ~stream_vt_cons(x1, xs) =>
   if step <= 0
     then stream_vt_cons(x1, aux1(xs, x1, step0))
     else stream_vt_cons(x0, aux1(xs, x0, step-1))
   // end of [if]
), lazy_vt_free(xs)
)
//
} (* end of [stream_vt_stepize] *)
//
(* ****** ****** *)

local

#include "./../mydepies.hats"

in (* in-of-local *)

implement
fileref_csv_parse_line_nerr
  (inp, nerr) = lines where
{
//
val keys =
fileref_get_line_string(inp)
val keys =
$CSVPARSE.csv_parse_line_nerr(keys, nerr)
val keys =
$UN.castvwtp0{stringlst_vt}(keys)
//
val lines = streamize_fileref_line(inp)
//
val keys2 = g0ofg1($UN.list_vt2t(keys))
val lines = keys_lines_csv_parse_line_nerr(keys2, lines, nerr)
//
val ((*freed*)) = list_vt_free(keys)
//
} // end of [fileref_csv_parse_line_nerr]

(* ****** ****** *)

implement
keys_lines_csv_parse_line_nerr
( keys
, lines, nerr) = let
//
val
cap = 2*length(keys)+1
//
fun
auxmain
(
xs: stream_vt(string)
,
rs: List0_vt(gvhashtbl)
,
nerr: &int >> _
) : List0_vt(gvhashtbl) =
(
//
case+ !xs of
| ~stream_vt_nil
    ((*void*)) => rs
| ~stream_vt_cons(x, xs) => let
//
    val r0 =
    gvhashtbl_make_nil(cap)
//
    fun
    auxlst
    ( ks: list0(string)
    , vs: List0_vt(Strptr1)): void =
    (
      case+ ks of
      | list0_nil() =>
        strptrlst_free(vs)
      | list0_cons(k, ks) =>
        (
          case+ vs of
          | ~list_vt_nil() => ()
          | ~list_vt_cons(v, vs) => let
              val v =
              strptr2string(v)
              val gv = GVstring(v)
              val () =
              gvhashtbl_set_atkey(r0, k, gv) in auxlst(ks, vs)
            end // end of [list_vt_cons]
        )
    ) (* end of [auxlst] *)
    val () = auxlst(keys, vs) where
    {
    val vs =
    $CSVPARSE.csv_parse_line_nerr(x, nerr)
    }
  in
    auxmain(xs, list_vt_cons(r0, rs), nerr)
  end
//
) (* end of [auxmain] *)
//
in
  list_vt_reverse(auxmain(lines, list_vt_nil(), nerr))
end (* end of [keys_lines_csv_parse_line_nerr] *)

end // end of [local]

(* ****** ****** *)

(* end of [utils.dats] *)
