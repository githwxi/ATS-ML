(* ****** ****** *)
(*
** For supporting
** basis statical computation
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "ATS-ML"
//
(* ****** ****** *)
//
#define ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)
//
#staload _ =
"libats/libc/DATS/math.dats"
#staload MATH =
"libats/libc/SATS/math.sats"
//
(* ****** ****** *)
//
#staload "./../SATS/stats.sats"
//
(* ****** ****** *)
//
staload UN = $UNSAFE
//
(* ****** ****** *)
//
typedef dblist = List(double)
//
(* ****** ****** *)

implement
{a}(*tmp*)
glist_max(xs) =
loop
( xs
, gdata_eval<a>(x)
) where
{
//
val+list_cons(x, xs) = xs
//
fun
loop
(
xs: List(a), x0: double
) : double =
(
case+ xs of
| list_nil() => x0
| list_cons(x, xs) =>
  loop(xs, max(x0, gdata_eval<a>(x)))
)
//
} (* end of [glist_max] *)

(* ****** ****** *)

implement
{a}(*tmp*)
glist_min(xs) =
loop
( xs
, gdata_eval<a>(x)
) where
{
//
val+list_cons(x, xs) = xs
//
fun
loop
(
xs: List(a), x0: double
) : double =
(
case+ xs of
| list_nil() => x0
| list_cons(x, xs) =>
  loop(xs, min(x0, gdata_eval<a>(x)))
)
//
} (* end of [glist_min] *)

(* ****** ****** *)

implement
{a}(*tmp*)
glistpre_accret
  (xs, n0) = let
//
fun
loop
(
xs: List(a), i: int, ret: double
) : double =
if
(i < n0)
then
(
case+ xs of
| list_nil() => ret
| list_cons(x, xs) =>
  loop(xs, i+1, ret*(1+gdata_eval<a>(x)))
)
else ret // end of [else]
//
in
  loop(xs, 0, 1.0) - 1.0
end // end of [glistpre_accret]

(* ****** ****** *)

implement
{a}(*tmp*)
glist_median
  (xs) = med where
{
//
val xs =
list_mergesort_fun<a>
( xs
, lam(x, y) =>
  let
    val x =
    gdata_eval<a>(x)
    and y =
    gdata_eval<a>(y) in compare(x, y)
  end // end of [let]
)
//
val xs2 = $UN.list_vt2t(xs)
val med = glistord_median<a>(xs2)
val ((*void*)) = list_vt_free(xs)
//
} (* end of [glist_median] *)

(* ****** ****** *)

implement
{a}(*tmp*)
glistord_median
  (xs) = let
//
val n0 = length(xs)
//
in
//
if
(nmod(n0, 2) > 0)
then let
  val x0 =
  list_get_at(xs, n0/2)
in
  gdata_eval<a>(x0)
end // end of [then]
else let
  val xs =
  list_drop(xs, half(n0)-1)
  val+list_cons(x0, xs) = xs
  val+list_cons(x1, xs) = xs
  val x0 = gdata_eval<a>(x0)
  val x1 = gdata_eval<a>(x1)
in
  (x0 + x1) / 2
end // end of [else]
//
end // end of [glistord_median]

(* ****** ****** *)

implement
{a}(*tmp*)
glist_mean
  (xs) =
  loop(xs, 1, x) where
{
//
val+
list_cons(x, xs) = xs
//
val x = gdata_eval<a>(x)
//
fun
loop
(
xs: List(a), n: int, tot: double
) : double =
(
case+ xs of
| list_nil() =>
  ( tot / n )
| list_cons(x, xs) =>
  loop(xs, n+1, tot + gdata_eval<a>(x))
)
//
} (* end of [glist_mean] *)

(* ****** ****** *)

implement
{a}(*tmp*)
glistpre_mean
  (xs, n0) =
  loop(xs, 1, x) where
{
//
val+
list_cons(x, xs) = xs
val x = gdata_eval<a>(x)
//
fun
loop
(
xs: List(a), n: int, tot: double
) : double =
(
if
(n < n0)
then
(
case+ xs of
| list_nil
    () => (tot / n)
  // list_nil
| list_cons
    (x, xs) =>
    loop(xs, n+1, tot + gdata_eval<a>(x))
  // end of [list_cons]
)
else (tot / n0)
)
//
} (* end of [glistpre_mean] *)

(* ****** ****** *)


implement
{a}(*tmp*)
glist_stdev
  (xs) = let
//
val n0 = list_length(xs)
//
local
implement
list_foldleft$fopr<double><a>
  (res, x) =
  res+gdata_eval(x)
in
val tot =
  list_foldleft<double><a>(xs, 0.0)
end // end of [local]
//
val mu = tot / n0
//
fun sq(x: double) = x*x
//
local
implement
list_foldleft$fopr<double><a>
  (res, x) =
  res+sq(gdata_eval<a>(x)-mu)
in
val sqsum =
  list_foldleft<double><a>(xs, 0.0)
end // end of [local]
//
in
  $MATH.sqrt(sqsum / n0) // n0 >= 1
end // end of [glist_stdev]

(* ****** ****** *)

implement
{a}(*tmp*)
glistpre_stdev
  (xs, n0) = let
//
fun sq(x: double) = x*x
//
val mu =
glistpre_mean<a>(xs, n0)
//
fun
auxlst
( xs: List(a)
, i0: int, sqsum: double): double =
(
if
(i0 < n0)
then
(
case+ xs of
| list_nil() =>
  $MATH.sqrt(sqsum / i0)
| list_cons(x, xs) =>
  auxlst
  ( xs, i0+1
  , sqsum+sq(gdata_eval<a>(x)-mu))
)
else
(
  $MATH.sqrt(sqsum / (n0)) // n0 >= 1
)
) (* end of [auxlst] *)
//
in
  auxlst(xs, 0, 0.0)
end // end of [glistpre_stdev]

(* ****** ****** *)

implement
{a}(*tmp*)
glist_absdev
  (xs) = let
//
val n0 = list_length(xs)
//
local
implement
list_foldleft$fopr<double><a>
  (res, x) =
  res+gdata_eval<a>(x)
in
val tot =
  list_foldleft<double><a>(xs, 0.0)
end // end of [local]
//
val mu = tot / n0
//
local
implement
list_foldleft$fopr<double><a>
  (res, x) =
  res+abs(gdata_eval<a>(x)-mu)
in
val abssum =
  list_foldleft<double><a>(xs, 0.0)
end // end of [local]
//
in
  (abssum / (n0)) // n0 >= 1
end // end of [glist_absdev]

(* ****** ****** *)

implement
{a}(*tmp*)
glistpre_absdev
  (xs, n0) = let
//
val mu = glistpre_mean<a>(xs, n0)
//
fun
auxlst
( xs: List(a)
, i0: int, abssum: double): double =
(
if
(i0 < n0)
then
(
case+ xs of
| list_nil() =>
  (abssum / i0)
| list_cons(x, xs) => let
    val x = gdata_eval<a>(x)
  in
    auxlst(xs, i0+1, abssum+abs(x-mu))
  end // end of [list_cons]
)
else
(
  abssum / (n0) // n0 >= 1
)
) (* end of [auxlst] *)
//
in
  auxlst(xs, 0, 0.0)
end // end of [glistpre_absdev]

(* ****** ****** *)

implement
{a}(*tmp*)
glist_maxdrawdown
  (xs) = let
//
fun
auxlst
(
  xs: List(a), res: double
) : double =
(
case+ xs of
| list_nil() => res
| list_cons(x0, xs) => let
    val x0 = gdata_eval<a>(x0)
    fun
    aux
    (xs: List(a), lo: double): double =
    (
      case+ xs of
      | list_nil() => lo
      | list_cons(x1, xs) => let
          val x1 = gdata_eval<a>(x1)
        in
          if x1 < lo
            then aux(xs, x1)
            else (if x1 < x0 then aux(xs, lo) else lo)
          // end of [if]
        end // end of [list_cons]
    )
    val
    res2 = aux(xs, x0)/x0
  in
    if res <= res2 then auxlst(xs, res) else auxlst(xs, res2)
  end // end of [list_cons]
)
//
in
  auxlst(xs, 1.0(*100%*)) - 1.0
end // end of [glist_maxdrawdown]

(* ****** ****** *)

implement
{a}(*tmp*)
list_rolling_stream
{n}{k}(xs, width) = let
//
fun
auxlst
{n1,n2:nat|n1==n2+k}
(
xs: list(a, n1), ys: list(a, n2)
) : stream_vt(listGte(a,k)) = $ldelay
(
//
case+ ys of
| list_nil() =>
  stream_vt_sing(xs)
| list_cons(y, ys) =>
  stream_vt_cons(xs, auxlst(list_tail(xs), ys))
//
)
//
in
  auxlst(xs, list_drop(xs, width))
end // end of [list_rolling_stream]

(* ****** ****** *)

(* end of [stats_tmp.dats] *)
