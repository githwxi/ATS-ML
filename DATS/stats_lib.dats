(* ****** ****** *)
(*
** For supporting
** basis statical computation
*)
(* ****** ****** *)
//
#define
ATS_PACKNAME "ATS-ML"
//
(* ****** ****** *)
//
#define ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)
//
#staload _ =
"libats/libc/DATS/math.dats"
#staload MATH =
"libats/libc/SATS/math.sats"
//
(* ****** ****** *)
//
#staload "./../SATS/stats.sats"
//
(* ****** ****** *)
//
#staload "./../DATS/stats_tmp.dats"
//
(* ****** ****** *)

implement
//{}(*tmp*)
list_max(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_max<double>(xs)
end (* end of [list_max] *)

implement
//{}(*tmp*)
list_min(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_min<double>(xs)
end (* end of [list_min] *)

(* ****** ****** *)

implement
//{}(*tmp*)
list_median(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_median<double>(xs)
end (* end of [list_median] *)

(* ****** ****** *)

implement
//{}(*tmp*)
listord_median(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glistord_median<double>(xs)
end (* end of [listord_median] *)

(* ****** ****** *)

implement
//{}(*tmp*)
list_mean(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_mean<double>(xs)
end (* end of [list_mean] *)

implement
//{}(*tmp*)
listpre_mean(xs, n) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glistpre_mean<double>(xs, n)
end (* end of [listpre_mean] *)

(* ****** ****** *)

implement
//{}(*tmp*)
list_stdev(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_stdev<double>(xs)
end (* end of [list_stdev] *)

implement
//{}(*tmp*)
listpre_stdev(xs, n) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glistpre_stdev<double>(xs, n)
end (* end of [listpre_stdev] *)

(* ****** ****** *)

implement
//{}(*tmp*)
list_absdev(xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_absdev<double>(xs)
end (* end of [list_absdev] *)

implement
//{}(*tmp*)
listpre_absdev(xs, n) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glistpre_absdev<double>(xs, n)
end (* end of [listpre_absdev] *)

(* ****** ****** *)

implement
//{}(*tmp*)
list_maxdrawdown
  (xs) = let
//
implement
gdata_eval<double>(x) = x
//
in
  glist_maxdrawdown<double>(xs)
end // end of [list_maxdrawdown]

(* ****** ****** *)

implement
list_summarize(xs) =
{
//
val () = let
  val size = length(xs)
in
//
println!
("list_summarize: size=", size)
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val max = list_max(xs)
in
//
println!
  ("list_summarize: max=", max)
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val min = list_min(xs)
in
//
println!
  ("list_summarize: min=", min)
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val med = list_median(xs)
in
//
println!
  ("list_summarize: med=", med)
//
end // end of [val]
//
(*
val () =
if
(xs >= 1)
then let
  val MDD =
    list_maxdrawdown(xs)
  // end of [val]
in
//
println!
  ("list_summarize: MDD=", MDD)
//
end // end of [val]
*)
//
val () =
if
(xs >= 1)
then let
  val mean = list_mean(xs)
in
//
println!
  ("list_summarize: mean=", mean)
//
end // end of [val]
//
val () =
if
(xs >= 1)
then let
  val stdev = list_stdev(xs)
in
//
println!
  ("list_summarize: stdev=", stdev)
//
end // end of [if] // end of [val]
//
val () =
if
(xs >= 1)
then let
  val absdev = list_absdev(xs)
in
//
println!
  ("list_summarize: absdev=", absdev)
//
end // end of [if] // end of [val]
//
} (* end of [list_summarize] *)

(* ****** ****** *)

implement
list_ratios(xs) =
  auxlst(x0, xs) where
{
//
fun
auxlst
(
x0: double,
xs: List(double)
) : stream_vt(double) = $ldelay
(
case+ xs of
| list_nil() =>
  stream_vt_nil()
| list_cons(x1, xs) =>
  stream_vt_cons(x1/x0, auxlst(x1, xs))
)
//
val+list_cons(x0, xs) = xs
//
} (* end of [list_ratios] *)

implement
list_change_ratios(xs) =
  auxlst(x0, xs) where
{
//
fun
auxlst
(
x0: double,
xs: List(double)
) : stream_vt(double) = $ldelay
(
case+ xs of
| list_nil() =>
  stream_vt_nil()
| list_cons(x1, xs) =>
  stream_vt_cons(x1/x0 - 1.0, auxlst(x1, xs))
)
//
val+list_cons(x0, xs) = xs
//
} (* end of [list_change_ratios] *)

(* ****** ****** *)

implement
//{}(*tmp*)
list_rolling_mean
{n}{k}(xs, width) = let
//
fun
auxlst
{n1,n2:nat|n1==n2+k}
( xs: list(double, n1)
, ys: list(double, n2)
) : stream_vt(double) = $ldelay
(
let
//
val
mean = listpre_mean(xs, width)
//
in
//
case+ ys of
| list_nil() =>
  stream_vt_sing(mean)
| list_cons(y, ys) =>
  stream_vt_cons(mean, auxlst(list_tail(xs), ys))
//
end
)
//
in
  auxlst(xs, list_drop(xs, width))
end // end of [list_rolling_mean]

(* ****** ****** *)

implement
//{}(*tmp*)
list_rolling_stdev
{n}{k}(xs, width) = let
//
fun
auxlst
{n1,n2:nat|n1==n2+k}
( xs: list(double, n1)
, ys: list(double, n2)
) : stream_vt(double) = $ldelay
(
let
//
val
stdev = listpre_stdev(xs, width)
//
in
//
case+ ys of
| list_nil() =>
  stream_vt_sing(stdev)
| list_cons(y, ys) =>
  stream_vt_cons(stdev, auxlst(list_tail(xs), ys))
//
end
)
//
in
  auxlst(xs, list_drop(xs, width))
end // end of [list_rolling_stdev]

(* ****** ****** *)

(* end of [stats_lib.dats] *)
