(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload
MATH =
"libats/libc/SATS/math.sats"
//
staload _ =
"libats/libc/DATS/math.dats"
//
(* ****** ****** *)
//
#define
ATSML_targetloc "./.."
//
(* ****** ****** *)
//
#include
"{$ATSML}/mylibies.hats"
//
(* ****** ****** *)

implement
main0() = () where
{
//
var
nerr: int = 0
//
val
lines =
$UTILS.fileref_csv_parse_line_nerr
  (stdin_ref, nerr)
//
val
((*void*)) =
auxlst(lines) where
{
fun
auxlst
( lines
: List_vt(gvhashtbl)): void =
(
case+ lines of
| ~list_vt_nil() => ()
| ~list_vt_cons
    (line, lines) =>
  (
    fprintln!(stdout_ref, line); auxlst(lines)
  )
)
}
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test01.dats] *)
